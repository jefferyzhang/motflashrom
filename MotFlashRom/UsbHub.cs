﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SamsungOdin
{
    class UsbHub
    {
        #region win32 api
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr CreateFile(string lpFileName, uint dwDesiredAccess, int dwShareMode, IntPtr lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, IntPtr hTemplateFile);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr hObject);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool DeviceIoControl(IntPtr hDevice, int dwIoControlCode, IntPtr lpInBuffer, int nInBufferSize, IntPtr lpOutBuffer, int nOutBufferSize, out int lpBytesReturned, IntPtr lpOverlapped);
        #region constants
        public const int IOCTL_GET_HCD_DRIVERKEY_NAME = 0x220424;
        public const int IOCTL_USB_GET_ROOT_HUB_NAME = 0x220408;
        public const int IOCTL_USB_GET_NODE_INFORMATION = 0x220408;
        public const int IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX = 0x220448;
        public const int IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION = 0x220410;
        public const int IOCTL_USB_GET_NODE_CONNECTION_NAME = 0x220414;
        public const int IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME = 0x220420;
        public const int IOCTL_STORAGE_GET_DEVICE_NUMBER = 0x2D1080;
        public const int MAXIMUM_USB_STRING_LENGTH = 255;
        public const int MAX_BUFFER_SIZE = 2048;
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint GENERIC_EXECUTE = 0x20000000;
        public const uint GENERIC_ALL = 0x10000000;
        public const uint FILE_FLAG_NO_BUFFERING = 0x20000000;
        public const Int32 FILE_ATTRIBUTE_NORMAL = 0X80;
        public const Int32 FILE_FLAG_OVERLAPPED = 0X40000000;
        public const int FILE_SHARE_READ = 0x1;
        public const int FILE_SHARE_WRITE = 0x2;
        public const int OPEN_EXISTING = 0x3;
        public const int INVALID_HANDLE_VALUE = -1;
        public enum USB_HUB_NODE
        {
            UsbHub,
            UsbMIParent
        }
        public enum USB_DESCRIPTOR_TYPE : byte
        {
            DeviceDescriptorType = 0x1,
            ConfigurationDescriptorType = 0x2,
            StringDescriptorType = 0x3,
            InterfaceDescriptorType = 0x4,
            EndpointDescriptorType = 0x5,
            HubDescriptor = 0x29
        }
        public enum UsbDeviceClass : byte
        {
            UnspecifiedDevice = 0x00,
            AudioInterface = 0x01,
            CommunicationsAndCDCControlBoth = 0x02,
            HIDInterface = 0x03,
            PhysicalInterfaceDevice = 0x5,
            ImageInterface = 0x06,
            PrinterInterface = 0x07,
            MassStorageInterface = 0x08,
            HubDevice = 0x09,
            CDCDataInterface = 0x0A,
            SmartCardInterface = 0x0B,
            ContentSecurityInterface = 0x0D,
            VidioInterface = 0x0E,
            PersonalHeathcareInterface = 0x0F,
            DiagnosticDeviceBoth = 0xDC,
            WirelessControllerInterface = 0xE0,
            MiscellaneousBoth = 0xEF,
            ApplicationSpecificInterface = 0xFE,
            VendorSpecificBoth = 0xFF
        }
        public enum USB_DEVICE_SPEED : byte
        {
            UsbLowSpeed,
            UsbFullSpeed,
            UsbHighSpeed
        }
        public enum USB_CONNECTION_STATUS : int
        {
            NoDeviceConnected,
            DeviceConnected,
            DeviceFailedEnumeration,
            DeviceGeneralFailure,
            DeviceCausedOvercurrent,
            DeviceNotEnoughPower,
            DeviceNotEnoughBandwidth,
            DeviceHubNestedTooDeeply,
            DeviceInLegacyHub
        }
        #endregion
        #region struct
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct USB_HUB_DESCRIPTOR
        {
            public byte bDescriptorLength;
            public USB_DESCRIPTOR_TYPE bDescriptorType;
            public byte bNumberOfPorts;
            public short wHubCharacteristics;
            public byte bPowerOnToPowerGood;
            public byte bHubControlCurrent;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
            public byte[] bRemoveAndPowerMask;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct USB_HUB_INFORMATION
        {
            public USB_HUB_DESCRIPTOR HubDescriptor;
            public bool HubIsBusPowered;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct USB_NODE_INFORMATION
        {
            //public int NodeType;
            public USB_HUB_NODE NodeType;
            public USB_HUB_INFORMATION HubInformation;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public class USB_DEVICE_DESCRIPTOR
        {
            public byte bLength;
            public USB_DESCRIPTOR_TYPE bDescriptorType;
            public short bcdUSB;
            public UsbDeviceClass bDeviceClass;
            public byte bDeviceSubClass;
            public byte bDeviceProtocol;
            public byte bMaxPacketSize0;
            public ushort idVendor;
            public ushort idProduct;
            public short bcdDevice;
            public byte iManufacturer;
            public byte iProduct;
            public byte iSerialNumber;
            public byte bNumConfigurations;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct USB_NODE_CONNECTION_INFORMATION_EX
        {
            public int ConnectionIndex;
            public USB_DEVICE_DESCRIPTOR DeviceDescriptor;
            public byte CurrentConfigurationValue;
            public USB_DEVICE_SPEED Speed;
            public byte DeviceIsHub;
            public short DeviceAddress;
            public int NumberOfOpenPipes;
            public USB_CONNECTION_STATUS ConnectionStatus;
            //add by steven for usb 3.0
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAXIMUM_USB_STRING_LENGTH)]
            public string Reserved;
            //public IntPtr PipeList;
        }
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct USB_NODE_CONNECTION_DRIVERKEY_NAME               // Yes, this is the same as the structure above...
        {
            public int ConnectionIndex;
            public int ActualLength;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_BUFFER_SIZE)]
            public string DriverKeyName;
        }
        #endregion
        #endregion
        String _hub_name = "";
        USB_NODE_INFORMATION _hub_info = new USB_NODE_INFORMATION();

        public UsbHub(String hubName)
        {
            _hub_name = hubName.ToLower();
            if (!_hub_name.StartsWith("usb"))
            {
                int pos = _hub_name.IndexOf("usb");
                if (pos > 0)
                {
                    _hub_name = _hub_name.Substring(pos);
                }
            }
        }
        public bool exist()
        {
            bool ret = false;
            try
            {
                string devicePath = @"\\?\" + _hub_name;
                IntPtr handel2 = CreateFile(devicePath, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                if (handel2.ToInt32() != INVALID_HANDLE_VALUE)
                {
                    _hub_info.NodeType = USB_HUB_NODE.UsbHub;
                    int nBytes = Marshal.SizeOf(_hub_info);
                    IntPtr ptrNodeInfo = Marshal.AllocHGlobal(nBytes);
                    Marshal.StructureToPtr(_hub_info, ptrNodeInfo, true);
                    int nBytesReturned = -1;
                    if (DeviceIoControl(handel2, IOCTL_USB_GET_NODE_INFORMATION, ptrNodeInfo, nBytes, ptrNodeInfo, nBytes, out nBytesReturned, IntPtr.Zero))
                    {
                        _hub_info = (USB_NODE_INFORMATION)Marshal.PtrToStructure(ptrNodeInfo, typeof(USB_NODE_INFORMATION));
                        ret = true;
                    }
                    CloseHandle(handel2);
                }
            }
            catch (Exception) { }
            return ret;
        }
        public String getDriverKeyByPort(int port)
        {
            String ret = "";
            try
            {
                if (_hub_info.HubInformation.HubDescriptor.bNumberOfPorts > 0 && port <= _hub_info.HubInformation.HubDescriptor.bNumberOfPorts)
                {
                    int nBytesReturned = -1;
                    string devicePath = @"\\?\" + _hub_name;
                    IntPtr handel2 = CreateFile(devicePath, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                    if (handel2.ToInt32() != INVALID_HANDLE_VALUE)
                    {
                        // connection status
                        USB_NODE_CONNECTION_INFORMATION_EX nodeConnection = new USB_NODE_CONNECTION_INFORMATION_EX();
                        nodeConnection.ConnectionIndex = port;
                        int nBytes = Marshal.SizeOf(typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                        IntPtr ptrNodeConnection = Marshal.AllocHGlobal(nBytes);
                        Marshal.StructureToPtr(nodeConnection, ptrNodeConnection, true);
                        if (DeviceIoControl(handel2, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, ptrNodeConnection, nBytes, ptrNodeConnection, nBytes, out nBytesReturned, IntPtr.Zero))
                        {
                            nodeConnection = (USB_NODE_CONNECTION_INFORMATION_EX)Marshal.PtrToStructure(ptrNodeConnection, typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                        }
                        Marshal.FreeHGlobal(ptrNodeConnection);

                        if (nodeConnection.ConnectionStatus == USB_CONNECTION_STATUS.DeviceConnected)
                        {
                            // driver key
                            USB_NODE_CONNECTION_DRIVERKEY_NAME DriverKey = new USB_NODE_CONNECTION_DRIVERKEY_NAME();
                            DriverKey.ConnectionIndex = port;
                            nBytes = Marshal.SizeOf(DriverKey);
                            nBytesReturned = -1;
                            IntPtr ptrDriverKey = Marshal.AllocHGlobal(nBytes);
                            Marshal.StructureToPtr(DriverKey, ptrDriverKey, true);
                            if (DeviceIoControl(handel2, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, ptrDriverKey, nBytes, ptrDriverKey, nBytes, out nBytesReturned, IntPtr.Zero))
                            {
                                DriverKey = (USB_NODE_CONNECTION_DRIVERKEY_NAME)Marshal.PtrToStructure(ptrDriverKey, typeof(USB_NODE_CONNECTION_DRIVERKEY_NAME));
                                ret = DriverKey.DriverKeyName;
                            }
                            Marshal.FreeHGlobal(ptrDriverKey);
                        }
                        CloseHandle(handel2);
                    }
                }
            }
            catch (Exception) { }
            return ret;
        }
        public USB_NODE_CONNECTION_INFORMATION_EX getUsbDeviceInfomationByPort(int port)
        {
            USB_NODE_CONNECTION_INFORMATION_EX ret = new USB_NODE_CONNECTION_INFORMATION_EX();
            try
            {
                if (_hub_info.HubInformation.HubDescriptor.bNumberOfPorts > 0 && port <= _hub_info.HubInformation.HubDescriptor.bNumberOfPorts)
                {
                    int nBytesReturned = -1;
                    string devicePath = @"\\?\" + _hub_name;
                    IntPtr handel2 = CreateFile(devicePath, GENERIC_WRITE, FILE_SHARE_WRITE, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
                    if (handel2.ToInt32() != INVALID_HANDLE_VALUE)
                    {
                        // connection status
                        USB_NODE_CONNECTION_INFORMATION_EX nodeConnection = new USB_NODE_CONNECTION_INFORMATION_EX();
                        nodeConnection.ConnectionIndex = port;
                        int nBytes = Marshal.SizeOf(typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                        IntPtr ptrNodeConnection = Marshal.AllocHGlobal(nBytes);
                        Marshal.StructureToPtr(nodeConnection, ptrNodeConnection, true);
                        if (DeviceIoControl(handel2, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, ptrNodeConnection, nBytes, ptrNodeConnection, nBytes, out nBytesReturned, IntPtr.Zero))
                        {
                            ret = (USB_NODE_CONNECTION_INFORMATION_EX)Marshal.PtrToStructure(ptrNodeConnection, typeof(USB_NODE_CONNECTION_INFORMATION_EX));
                        }
                        Marshal.FreeHGlobal(ptrNodeConnection);
                        CloseHandle(handel2);
                    }
                }
            }
            catch (Exception) { }
            return ret;
        }
    }
}
