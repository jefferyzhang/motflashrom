﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashRom
{
    class RunExe
    {
        static String _TAG = "[MOTFLASH]";
        public static String TAG
        {
            set
            {
                _TAG = value;
            }
        }
        public static void logIt(String msg)
        {
            //System.Diagnostics.Trace.WriteLine(String.Format("{0}: {1}", _TAG, msg));
            Console.WriteLine(msg);
        }

        public static void dumpVersion()
        {
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            logIt(string.Format("version={0}", myFileVersionInfo.FileVersion));
            logIt(string.Format("args={0}", System.Environment.CommandLine));
        }


        public static Boolean CheckReturnStrs(String[] ss, String s)
        {
            Boolean ret = false;
            foreach(String sa in ss)
            {
                if (sa.IndexOf(s)!=-1)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }

        public static Tuple<string[], int> runExe(string exeFilename, string param, int timeout = 120*1000, string workingDir = "")
        {
            List<string> ret = new List<string>();
            int exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    DateTime last_output = DateTime.Now;
                    DateTime _start = DateTime.Now;
                    System.Threading.ManualResetEvent ev = new System.Threading.ManualResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = param;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    if (!string.IsNullOrEmpty(workingDir))
                        p.StartInfo.WorkingDirectory = workingDir;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.CreateNoWindow = true;
                    //p.EnableRaisingEvents = true;
                    p.OutputDataReceived += (obj, args) =>
                    {
                        last_output = DateTime.Now;
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe]: {0}", args.Data));
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };

                    p.ErrorDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe,Error]: {0}", args.Data));
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };
                    //p.Exited += (o, e) => { ev.Set(); };
                    p.Start();
                    _start = DateTime.Now;
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();
                    bool process_terminated = false;
                    bool proces_stdout_cloded = false;
                    bool proces_has_killed = false;
                    while (!proces_stdout_cloded || !process_terminated)
                    {
                        if (p.HasExited)
                        {
                            // process is terminated
                            process_terminated = true;
                            //logIt(string.Format("[runExe]: process is going to terminate."));
                        }
                        if (ev.WaitOne(1000))
                        {
                            // stdout is colsed
                            proces_stdout_cloded = true;
                            //logIt(string.Format("[runExe]: stdout pipe is going to close."));
                        }
                        if ((DateTime.Now - last_output).TotalMilliseconds > timeout)
                        {
                            logIt(string.Format("[runExe]: there are {0} milliseconds no response. timeout?", timeout));
                            // no output received within timeout milliseconds
                            if (!p.HasExited)
                            {
                                exitCode = 1460;
                                p.Kill();
                                proces_has_killed = true;
                                logIt(string.Format("[runExe]: process is going to be killed due to timeout."));
                            }
                        }
                    }
                    if (!proces_has_killed)
                        exitCode = p.ExitCode;
                }
                else
                {
                    logIt(string.Format("[runExe]: {0} not exist.", exeFilename));
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return Tuple.Create(ret.ToArray(), exitCode);
        }

        public static Tuple<string[], int> runExeV1(string exeFilename, string param,  int timeout = 120*1000)
        {
            List<string> ret = new List<string>();
            int exitCode = 1;
            logIt(string.Format("[runExe]: ++ exe={0}, param={1}", exeFilename, param));
            try
            {
                if (System.IO.File.Exists(exeFilename))
                {
                    System.Threading.AutoResetEvent ev = new System.Threading.AutoResetEvent(false);
                    Process p = new Process();
                    p.StartInfo.FileName = exeFilename;
                    p.StartInfo.Arguments = param;
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.CreateNoWindow = true;
                    //p.EnableRaisingEvents = true;
                    DateTime dtStart = DateTime.Now;
                    p.OutputDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe,Output]: {0}", args.Data));
                            ret.Add(args.Data);
                            dtStart = DateTime.Now;
                        }
                        if (args.Data == null)
                            ev.Set();
                    };

                    p.ErrorDataReceived += (obj, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            logIt(string.Format("[runExe,Error]: {0}", args.Data));
                            ret.Add(args.Data);
                        }
                        if (args.Data == null)
                            ev.Set();
                    };

                    //p.Exited += (o, e) => { ev.Set(); };
                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();
                    if (p.WaitForExit(timeout))
                    {
                        while (!ev.WaitOne(timeout))
                        {
                            if((DateTime.Now-dtStart).TotalSeconds > 120)
                            {
                                break;
                            }
                        }
                        if (!p.HasExited)
                        {
                            exitCode = 1460;
                            p.Kill();
                        }
                        else
                            exitCode = p.ExitCode;
                    }
                    else
                    {
                        if (!p.HasExited)
                        {
                            p.Kill();
                        }
                        exitCode = 1460;
                    }
                }
            }
            catch (Exception ex)
            {
                logIt(string.Format("[runExe]: {0}", ex.Message));
                logIt(string.Format("[runExe]: {0}", ex.StackTrace));
            }
            logIt(string.Format("[runExe]: -- ret={0}", exitCode));
            return Tuple.Create(ret.ToArray(), exitCode);
        }
    }
}
