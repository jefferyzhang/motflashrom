﻿using FlashRom;
using SamsungOdin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace MotFlashRom
{
    class ERRORCODE
    {
        const int ERROR_BASE = 0;
        public const int ERROR_NOADB = ERROR_BASE + 1; // not find adb.exe
        public const int ERROR_ADBRUNFAIL = ERROR_BASE + 2;// not find .exe
        public const int ERROR_FASTBOOTRUNFAIL = ERROR_BASE + 3; // read oem unlock fail
        public const int ERROR_NOTFINDDEV = ERROR_BASE + 4;
        public const int ERROR_FLUSHROMFAIL = ERROR_BASE + 5;
        public const int ERROR_ROMMISMATCH = ERROR_BASE + 6;
        public const int ERROR_ROMNOTFOUND = ERROR_BASE + 7;
        public const int ERROR_ROMMD5 = ERROR_BASE + 8;
        public const int SUCCESS = 0;
    }
    class Program
    {
        static int waitForDeviceReboot(System.Collections.Specialized.StringDictionary args, Boolean bWaitfor = true, int time = 60)
        {
            int ret = 10;
            RunExe.logIt(string.Format("waitForDeviceReboot: ++ "));
            if (bWaitfor || !args.ContainsKey("nowaitforreboot"))
            {
                if (bWaitfor || args.ContainsKey("waitForDeviceRebootAfterFlashing"))
                {
                    int delay = 1; // default 60 seconds
                    if (!Int32.TryParse(args["waitForDeviceRebootAfterFlashing"], out delay))
                    {
                        delay = time;
                    }

                    if (delay > 0)
                    {
                        string hubName = args.ContainsKey("hubname") ? args["hubname"] : "";
                        int hubport = 0;
                        if (!Int32.TryParse(args["hubport"], out hubport))
                            hubport = 0;
                        if (String.IsNullOrEmpty(hubName) || hubport == 0)
                        {
                            RunExe.logIt(string.Format("waitForDeviceReboot: Not hub information. "));
                            return ret;
                        }
                        RunExe.logIt(string.Format("waitForDeviceReboot: waitForDeviceRebootAfterFlashing={0}. will wait for device reboot {1} seconds at {2}@{3}.", args["waitForDeviceRebootAfterFlashing"], delay, hubport, hubName));
                        DateTime _start = DateTime.Now;
                        bool reboot = false;
                        int phase = 1;
                        while (!reboot && (DateTime.Now - _start).TotalSeconds < delay && !string.IsNullOrEmpty(hubName) && hubport > 0)
                        {
                            UsbHub hub = new UsbHub(hubName);
                            if (hub.exist())
                            {
                                UsbHub.USB_NODE_CONNECTION_INFORMATION_EX info = hub.getUsbDeviceInfomationByPort(hubport);
                                RunExe.logIt(string.Format("waitForDeviceReboot: phase={0} status={1}.", phase, info.ConnectionStatus.ToString()));
                                switch (phase)
                                {
                                    case 1: // phase=1, means device just finished the flashing and rebooting.  (device still connected)                                      
                                        if (info.ConnectionStatus == UsbHub.USB_CONNECTION_STATUS.DeviceConnected)
                                        {
                                            // device still connected
                                        }
                                        else
                                        {
                                            // device gone
                                            phase = 2;
                                        }
                                        break;
                                    case 2: // phase=2, means device gone, after flashing and rebooting.
                                        if (info.ConnectionStatus == UsbHub.USB_CONNECTION_STATUS.DeviceConnected)
                                        {
                                            // device still connected
                                            phase = 3;
                                        }
                                        else
                                        {
                                            // device gone
                                            //phase = 2;
                                        }
                                        break;
                                    case 3: // phase=3, means device rebooted, after flashing and rebooting.
                                        if (info.ConnectionStatus == UsbHub.USB_CONNECTION_STATUS.DeviceConnected)
                                        {
                                            // device still connected
                                            //phase = 3;
                                            ret = 0;
                                            RunExe.logIt(string.Format("waitForDeviceReboot: phase={0} vid={1:X4}, pid={2:X4}.", phase, info.DeviceDescriptor.idVendor, info.DeviceDescriptor.idProduct));
                                            if (args.ContainsKey("verifyvidpid"))
                                            {
                                                if (string.Compare(args["verifyvidpid"], string.Format("{0:X4}{1:X4}", info.DeviceDescriptor.idVendor, info.DeviceDescriptor.idProduct), true) == 0)
                                                    ret = 0;
                                                else
                                                    ret = ERRORCODE.ERROR_NOTFINDDEV;
                                            }
                                            reboot = true;
                                        }
                                        else
                                        {
                                            // device gone
                                            //phase = 2;
                                        }
                                        break;
                                    default: break;
                                }
                            }
                            if (!reboot)
                                System.Threading.Thread.Sleep(1000);
                        }
                    }
                }
                else
                {
                    RunExe.logIt(string.Format("waitForDeviceReboot: missing key waitForDeviceRebootAfterFlashing. skip waiting for device reboot."));
                }
            }
            else
            {
                RunExe.logIt(string.Format("waitForDeviceReboot: command line include -nowaitforreboot args. skip waiting for device reboot."));
            }
            RunExe.logIt(string.Format("waitForDeviceReboot: -- ret={0}", ret));
            return ret;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:X2}", b);
            return hex.ToString();
        }

        static int Main(string[] args)
        {
            int ret = 87;
            System.Configuration.Install.InstallContext _args = new System.Configuration.Install.InstallContext(null, args);
            if (_args.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("wait for debug...");
                System.Console.ReadKey();
            }
            if (_args.Parameters.ContainsKey("label"))
            {
                RunExe.TAG = String.Format("[MOTFLASH][label_{0}]", _args.Parameters["label"]);
            }
            RunExe.dumpVersion();

            String sSerialno = "";
            String sadb = @"D:\AndroidDev\sdk\platform-tools\adb.exe";
            String motfastboot = @"D:\AndroidDev\sdk\platform-tools\mfastboot.exe";
            String sHubName = "USB#ROOT_HUB30#4&35658092&0&0#{f18a0e88-c30c-11d0-8815-00a0c906bed8}";
            int nPort = 9;
            String sRomPath = "";

            String sRomFile = @"";
            if (_args.Parameters.ContainsKey("rom"))
            {
                sRomFile = _args.Parameters["rom"];
                if(File.Exists(sRomFile))
                {
                    //unzip rom
                    Guid id = Guid.NewGuid();
                    sRomPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), string.Format("{0}_{1}", _args.Parameters["label"], id.ToString()));
                    //string unzip_file = System.IO.Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "unzip.exe");
                    string unzip_file = System.IO.Path.Combine(System.Environment.GetEnvironmentVariable("APSTHOME"), "7z.exe");
                    Tuple<string[], int> exeret = RunExe.runExe(unzip_file, string.Format("x \"{0}\" -o\"{1}\" -aoa -r", sRomFile, sRomPath));
                    if(exeret.Item2!=ERRORCODE.SUCCESS)
                    {
                        ret = exeret.Item2;
                        return ret;
                    }
                }
                else
                {
                    if (Directory.Exists(sRomFile))
                    {
                        sRomPath = sRomFile;
                    }
                }
            }
            else
            {
                ret = ERRORCODE.ERROR_ROMNOTFOUND;
                return ret;
            }

            if (_args.Parameters.ContainsKey("hubname"))
            {
                sHubName = _args.Parameters["hubname"];
            }

            if (_args.Parameters.ContainsKey("hubport"))
            {
                nPort = Convert.ToInt32(_args.Parameters["hubport"]);
            }
            if(!_args.IsParameterTrue("debug"))
            {
                if (_args.Parameters.ContainsKey("serialno"))
                {
                    sSerialno = _args.Parameters["serialno"];
                }
                if (_args.Parameters.ContainsKey("adb"))
                {
                    sadb = Environment.ExpandEnvironmentVariables(_args.Parameters["adb"]);
                }
                else
                {
                    sadb = Environment.ExpandEnvironmentVariables("%APSTHOME%adb.exe");
                }

                if (_args.Parameters.ContainsKey("fastboot"))
                {
                    motfastboot = Environment.ExpandEnvironmentVariables(_args.Parameters["fastboot"]);
                }
                else
                {
                    motfastboot = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "mfastboot.exe");
                }
            }

            String sFxml = Path.Combine(sRomPath, "flashfile.xml");
            if(!File.Exists(sFxml))
            {
                string[] files = System.IO.Directory.GetFiles(sRomPath, "*.xml");
                if(files.Length>0)
                {
                    sFxml = files[0];
                }
            }

            if(_args.IsParameterTrue("md5"))
            {
                if (File.Exists(sFxml))
                {
                    XmlDocument flashdoc = new XmlDocument();
                    flashdoc.Load(sFxml);
                    if (flashdoc != null && flashdoc.DocumentElement != null)
                    {
                        XmlNode steps = flashdoc.DocumentElement.SelectSingleNode("//steps[@interface='AP']");
                        foreach (XmlNode step in steps.ChildNodes)
                        {
                            if (step.NodeType == XmlNodeType.Comment) continue;
                            String sFilename = "";
                            String sMD5 = "";
                            if (step.Attributes != null && step.Attributes["filename"] != null)
                            {
                                sFilename = step.Attributes["filename"].Value;
                            }
                            if (step.Attributes != null && step.Attributes["MD5"] != null)
                            {
                                sMD5 = step.Attributes["MD5"].Value;
                            }
                            if(!String.IsNullOrEmpty(sFilename)&&!String.IsNullOrEmpty(sMD5))
                            {
                                if (File.Exists(Path.Combine(sRomPath, sFilename)))
                                {
                                    using (var md5 = MD5.Create())
                                    {
                                        using (var stream = File.OpenRead(Path.Combine(sRomPath, sFilename)))
                                        {
                                            byte[] aa = md5.ComputeHash(stream);
                                            if(String.Compare(ByteArrayToString(aa),sMD5, true)!=0)
                                            {
                                                return ERRORCODE.ERROR_ROMMD5;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    return ERRORCODE.ERROR_ROMMD5;
                                }
                            }
                        }
                    }
                }
                     
            }

            String sParam = String.IsNullOrEmpty(sSerialno) ? String.Format("devices") : String.Format("devices");
            int iRetry = 0;
            do
            {
                Tuple<string[], int> exeret = RunExe.runExe(sadb, sParam);
                if (exeret.Item2 != 0)
                {
                    ret = ERRORCODE.ERROR_ADBRUNFAIL;
                    break;
                }
                ret = exeret.Item2;
                if (String.IsNullOrEmpty(sSerialno) || RunExe.CheckReturnStrs(exeret.Item1, sSerialno)) break;
                Thread.Sleep(2000);
            } while (iRetry++ < 3);


            sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" reboot bootloader") : String.Format(" -s {0} reboot bootloader", sSerialno);
            Tuple<string[], int> exereta = RunExe.runExe(sadb, sParam);
            if (exereta.Item2 == ERRORCODE.SUCCESS)
            {
                ret = exereta.Item2;
            }

            //wait for devices
            //Thread.Sleep(5000);
            if (waitForDeviceReboot(_args.Parameters) != ERRORCODE.SUCCESS)
            {
                RunExe.logIt("wait device reboot failed.");
            }
            Thread.Sleep(2000);

            sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" -w") : String.Format(" -s {0} -w", sSerialno);
            RunExe.runExe(motfastboot, sParam, 20*1000);


            String sFlashxml = sFxml;// Path.Combine(sRomPath, "flashfile.xml");
            if(File.Exists(sFlashxml))
            {
                XmlDocument flashdoc = new XmlDocument();
                flashdoc.Load(sFlashxml);
                if (flashdoc != null && flashdoc.DocumentElement != null)
                {
                    XmlNode steps = flashdoc.DocumentElement.SelectSingleNode("//steps[@interface='AP']");
                    foreach(XmlNode step in steps.ChildNodes)
                    {
                        if (step.NodeType == XmlNodeType.Comment) continue;
                        if (step.Attributes != null && step.Attributes["operation"] != null)
                        {
                            string operation = step.Attributes["operation"].Value;
                            string sFilename = "";
                            string sMD5 = "";
                            string partition = "";
                            string var = "";
                            if (step.Attributes != null && step.Attributes["filename"] != null)
                            {
                                sFilename = step.Attributes["filename"].Value;
                            }
                            if (step.Attributes != null && step.Attributes["MD5"] != null)
                            {
                                sMD5 = step.Attributes["MD5"].Value;
                            }
                            if (step.Attributes != null && step.Attributes["partition"] != null)
                            {
                                partition = step.Attributes["partition"].Value;
                            }
                            if (step.Attributes != null && step.Attributes["var"] != null)
                            {
                                var = step.Attributes["var"].Value;
                            }
                            if(string.Compare("flash", operation, true)==0)
                            {
                                if (File.Exists(Path.Combine(sRomPath, sFilename)))
                                {
                                    sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" flash {0} {1}", partition, Path.Combine(sRomPath, sFilename)) :
                                       String.Format(" -s {0} flash {1} {2}", sSerialno, partition, Path.Combine(sRomPath, sFilename));
                                    Tuple<string[], int> exeret = RunExe.runExe(motfastboot, sParam, 10 * 60 * 1000);
                                    if (exeret.Item2 == ERRORCODE.SUCCESS)
                                    {
                                        if (!RunExe.CheckReturnStrs(exeret.Item1, "OKAY"))
                                        {
                                            ret = ERRORCODE.ERROR_FLUSHROMFAIL;
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (string.Compare("erase", operation, true)==0)
                            {
                                sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" erase {0}", partition) :
                                    String.Format(" -s {0} erase {1}", sSerialno, partition);
                                Tuple<string[], int> exeret = RunExe.runExe(motfastboot, sParam,6000);    
                                if(exeret.Item2 == ERRORCODE.SUCCESS)
                                {
                                    if(!RunExe.CheckReturnStrs(exeret.Item1, "OKAY"))
                                    {

                                    }
                                }
                            }
                            else if (string.Compare("getvar", operation, true) == 0)
                            {
                                sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" getvar {0}", var) :
                                    String.Format(" -s {0} getvar {1}", sSerialno, var);
                                Tuple<string[], int> exeret = RunExe.runExe(motfastboot, sParam, 2000);
                                if (exeret.Item2 == ERRORCODE.SUCCESS)
                                {
                                    if (!RunExe.CheckReturnStrs(exeret.Item1, "OKAY"))
                                    {

                                    }
                                }
                            }
                            else if (string.Compare("oem", operation, true) == 0)
                            {
                                sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" oem {0}", var) :
                                    String.Format(" -s {0} oem {1}", sSerialno, var);
                                Tuple<string[], int> exeret = RunExe.runExe(motfastboot, sParam, 2000);
                                if (exeret.Item2 == ERRORCODE.SUCCESS)
                                {
                                    if (!RunExe.CheckReturnStrs(exeret.Item1, "OKAY"))
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {

            }
            sParam = String.IsNullOrEmpty(sSerialno) ? String.Format(" reboot") : String.Format(" -s {0} reboot", sSerialno);
            RunExe.runExe(motfastboot, sParam);
            if (File.Exists(sRomFile))
            {
                try
                {
                    Directory.Delete(sRomPath, true);
                }catch(Exception)
                {

                }
            }
            waitForDeviceReboot(_args.Parameters, false);
            return ret;
        }
    }
}
